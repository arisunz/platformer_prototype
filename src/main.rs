use bevy::prelude::*;
use bevy_ecs_ldtk::prelude::*;
use bevy_rapier2d::prelude::*;
use platformer_proto::{
    camera::MainCameraPlugin, config::GameConfig, player::PlayerPlugin, world::WorldPlugin,
};

fn main() -> anyhow::Result<()> {
    let config = GameConfig::new()?;

    App::new()
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    window: WindowDescriptor {
                        width: config.window_width() as f32,
                        height: config.window_height() as f32,
                        cursor_visible: false,
                        title: "change me coward".to_string(),
                        resizable: false,
                        ..Default::default()
                    },
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0))
        .insert_resource(RapierConfiguration {
            gravity: Vec2::new(0.0, -750.0),
            ..default()
        })
        .add_plugin(RapierDebugRenderPlugin::default())
        .add_plugin(LdtkPlugin)
        .add_plugin(MainCameraPlugin)
        .add_plugin(WorldPlugin)
        .add_plugin(PlayerPlugin)
        .run();

    Ok(())
}
