use serde::Deserialize;

const DEFAULT_WINDOW_WIDTH: usize = 1200;
const DEFAULT_WINDOW_HEIGHT: usize = 600;

#[derive(Debug, Deserialize)]
pub struct GameConfig {
    window_width: Option<usize>,
    window_height: Option<usize>,
}

impl GameConfig {
    pub fn new() -> Result<Self, Error> {
        let conf = config::Config::builder()
            .add_source(config::File::with_name("config"))
            .build()
            .map_err(|_| Error::Builder)?;

        conf.try_deserialize().map_err(|_| Error::Read)
    }

    pub fn window_width(&self) -> usize {
        self.window_width.unwrap_or(DEFAULT_WINDOW_WIDTH)
    }

    pub fn window_height(&self) -> usize {
        self.window_height.unwrap_or(DEFAULT_WINDOW_HEIGHT)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("error creating a builder for reading configuration from file")]
    Builder,

    #[error("error reading/deserializing configuration")]
    Read,
}
