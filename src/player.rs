use bevy::{prelude::*, utils::HashSet};
use bevy_ecs_ldtk::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::{shared::components::ColliderBundle, world::GroundSensor};

// use crate::levels::SolidTile;

const HORIZONTAL_ACCELERATION: f32 = 200.0;
const MAX_VELOCITY: f32 = 250.0;
const JUMP_ACCELERATION: f32 = 300.0;

#[derive(Debug)]
pub struct PlayerPlugin;

#[derive(Debug, Default, Component)]
pub struct Player;

#[derive(Debug, Default, PartialEq, Eq, Component)]
pub enum JumpsLeft {
    #[default]
    Two,
    One,
    Zero,
}

impl JumpsLeft {
    pub fn reset(&mut self) {
        *self = JumpsLeft::Two;
    }

    pub fn decrease(&mut self) {
        *self = match self {
            JumpsLeft::Two => JumpsLeft::One,
            JumpsLeft::One => JumpsLeft::Zero,
            JumpsLeft::Zero => panic!("can't decrease zero-jump state"),
        };
    }
}

// #[derive(Debug, Component)]
// pub struct Grounded(bool);

// impl Default for Grounded {
//     fn default() -> Self {
//         Grounded(true)
//     }
// }

#[derive(Debug, Default, Component)]
pub struct ActiveGroundSensors {
    pub sensors: HashSet<Entity>,
}

#[derive(Bundle, Default, LdtkEntity)]
pub struct PlayerBundle {
    #[sprite_bundle("gfx/player.png")]
    #[bundle]
    pub sprite_bundle: SpriteBundle,

    #[worldly]
    pub worldly: Worldly,

    #[from_entity_instance]
    #[bundle]
    pub collider: ColliderBundle,

    pub player: Player,
    pub jump_state: JumpsLeft,
    // pub grounded: Grounded,
    pub ground_sensors: ActiveGroundSensors,
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.register_ldtk_entity::<PlayerBundle>("player")
            .add_system(player_movement_system)
            .add_system(ground_update_system);
    }
}

fn player_movement_system(
    keys: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut query: Query<(&mut Velocity, &mut JumpsLeft), With<Player>>,
) {
    if let Ok((mut vel, mut jumps)) = query.get_single_mut() {
        let delta = time.delta_seconds();

        let right = if keys.pressed(KeyCode::L) {
            HORIZONTAL_ACCELERATION
        } else {
            0.0
        };

        let left = if keys.pressed(KeyCode::J) {
            HORIZONTAL_ACCELERATION
        } else {
            0.0
        };

        let x = (right - left) * delta;

        vel.linvel.x += x;
        vel.linvel.x = vel.linvel.x.clamp(-MAX_VELOCITY, MAX_VELOCITY);

        if keys.just_pressed(KeyCode::Z) && *jumps != JumpsLeft::Zero {
            vel.linvel.y = JUMP_ACCELERATION;
            jumps.decrease();
        }
    }
}

fn ground_update_system(
    mut collision_reader: EventReader<CollisionEvent>,
    ground_sensors: Query<Entity, With<GroundSensor>>,
    mut player: Query<(Entity, &mut JumpsLeft, &mut ActiveGroundSensors), With<Player>>,
) {
    if let Ok((player_entity, mut jumps, mut active_sensors)) = player.get_single_mut() {
        for collision_event in collision_reader.iter() {
            match collision_event {
                CollisionEvent::Started(e1, e2, _) => {
                    if ground_sensors.contains(*e1) && player_entity == *e2 {
                        active_sensors.sensors.insert(*e1);
                    } else if ground_sensors.contains(*e2) && player_entity == *e1 {
                        active_sensors.sensors.insert(*e2);
                    }
                }
                CollisionEvent::Stopped(e1, e2, _) => {
                    if ground_sensors.contains(*e1) && player_entity == *e2 {
                        active_sensors.sensors.remove(e1);
                    } else if ground_sensors.contains(*e2) && player_entity == *e1 {
                        active_sensors.sensors.remove(e2);
                    }
                }
            }
        }

        // reset jump state if we're back back on the ground
        if !active_sensors.sensors.is_empty() {
            jumps.reset();
            active_sensors.sensors.clear();
        }
    }
}
