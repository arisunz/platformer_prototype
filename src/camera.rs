use bevy::prelude::*;

use crate::player::Player;

#[derive(Debug)]
pub struct MainCameraPlugin;

#[derive(Debug, Component)]
pub struct MainCamera;

impl Plugin for MainCameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(camera_creation_system)
            .add_system(player_follow_system);
    }
}

fn camera_creation_system(mut commands: Commands) {
    commands
        // .spawn(Camera2dBundle::default())
        .spawn(Camera2dBundle {
            projection: OrthographicProjection {
                scale: 0.5,
                ..default()
            },
            ..default()
        })
        .insert(MainCamera);
}

fn player_follow_system(
    player_query: Query<&Transform, With<Player>>,
    mut camera_query: Query<(&mut OrthographicProjection, &mut Transform), Without<Player>>,
) {
    if let Ok(player_pos) = player_query.get_single() {
        let (_camera_projection, mut camera_transform) = camera_query.single_mut();
        camera_transform.translation.x = player_pos.translation.x;
        camera_transform.translation.y = player_pos.translation.y;
    }
}
