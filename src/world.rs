use bevy::prelude::*;
use bevy_ecs_ldtk::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::shared::components::{ColliderBundle, SensorBundle};

#[derive(Debug)]
pub struct WorldPlugin;

#[derive(Debug, Default, Component)]
pub struct SolidTile;

#[derive(Bundle, LdtkEntity)]
pub struct SolidCollidablesBundle {
    pub solid: SolidTile,
}

#[derive(Debug, Default, Component)]
pub struct GroundSensor;

#[derive(Bundle, LdtkEntity)]
pub struct GroundSensorsBundle {
    pub sensor: GroundSensor,
}

impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(world_load_system)
            .insert_resource(LevelSelection::Identifier(String::from("level_0")))
            .insert_resource(LdtkSettings {
                set_clear_color: SetClearColor::FromLevelBackground,
                ..Default::default()
            })
            .register_ldtk_entity_for_layer::<SolidCollidablesBundle>("collisions", "collider")
            .register_ldtk_entity_for_layer::<GroundSensorsBundle>("floors", "floor")
            .add_system(insert_tile_colliders)
            .add_system(spawn_ground_sensors);
    }
}

fn world_load_system(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn(LdtkWorldBundle {
        ldtk_handle: asset_server.load("tilemaps/tilemap.ldtk"),
        ..default()
    });
}

fn insert_tile_colliders(mut commands: Commands, query: Query<Entity, Added<SolidTile>>) {
    for tile_entity in query.iter() {
        commands.entity(tile_entity).insert(ColliderBundle {
            collider: Collider::cuboid(16.0, 16.0),
            friction: Friction::new(1.0),
            rigid_body: RigidBody::Fixed,
            rotation_constraints: LockedAxes::ROTATION_LOCKED,
            ..default()
        });
    }
}

fn spawn_ground_sensors(
    mut commands: Commands,
    mut query: Query<(Entity, &mut Transform), Added<GroundSensor>>,
) {
    for (entity, mut transform) in query.iter_mut() {
        let old_translation = transform.translation;
        let new_translation = Vec3::new(old_translation.x, old_translation.y + 10.0, 0.0);
        transform.translation = new_translation;

        commands.entity(entity).insert(SensorBundle {
            collider: Collider::cuboid(7.0, 1.),
            sensor: Sensor,
            active_events: ActiveEvents::COLLISION_EVENTS,
            rotation_constraints: LockedAxes::ROTATION_LOCKED,
        });
    }
}
